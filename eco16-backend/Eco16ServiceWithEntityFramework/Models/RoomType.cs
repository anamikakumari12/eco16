﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eco16ServiceWithEntityFramework.Models
{
    public partial class RoomType
    {
        [Display(Name ="Room Type")]
        public string RoomType1 { get; set; }
        [Display(Name = "Avg. PM10")]
        public double Pm10 { get; set; }
        [Display(Name = "Avg. PM2.5")]
        public double Pm25 { get; set; }
        [Display(Name = "Avg. PM 1")]
        public double Pm1 { get; set; }
        public int Id { get; set; }
    }
}
