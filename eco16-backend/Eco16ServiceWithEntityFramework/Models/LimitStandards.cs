﻿using System.ComponentModel.DataAnnotations;

namespace Eco16ServiceWithEntityFramework.Models
{
    public partial class LimitStandards
    {
        [Display(Name = "Standard")]
        public string Standard { get; set; }
        [Display(Name = "Avg. PM10")]
        public double Pm10 { get; set; }
        [Display(Name = "Avg. PM2.5")]
        public double Pm25 { get; set; }
        [Display(Name = "Avg. PM1")]
        public double? Pm1 { get; set; }
        public int Id { get; set; }
    }
}
