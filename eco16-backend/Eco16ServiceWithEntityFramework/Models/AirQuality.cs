﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eco16ServiceWithEntityFramework.Models
{
    public partial class AirQuality
    {
        [Display(Name = "CC")]
        public string CountryCode { get; set; }
        public string Country { get; set; }
        public string Station { get; set; }
        public int Year { get; set; }
        [Display(Name = "Name and Type of monitoring stations")]
        public string NumberAndTypeOfMonitoringStations { get; set; }
        [Display(Name = "Note on converted PM10")]
        public string NoteOnConvertedPm10 { get; set; }
        [Display(Name = "Average PM10")]
        public double AveragePm10 { get; set; }
        [Display(Name = "Average PM2.5")]
        public double AveragePm25 { get; set; }
        [Display(Name = "Average PM1")]
        public double AveragePm1 { get; set; }
        [Display(Name = "Year of Measurement")]
        public int YearOfMeasurement { get; set; }
        public int Id { get; set; }
    }
}
