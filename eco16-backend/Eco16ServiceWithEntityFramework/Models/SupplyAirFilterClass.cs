﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eco16ServiceWithEntityFramework.Models
{
    public partial class SupplyAirFilterClass
    {
        [Display(Name = "Product Type")]
        public string ProductType { get; set; }
        [Display(Name = "Filter Class")]
        public string FilterClass { get; set; }
        [Display(Name = "Avg. PM10")]
        public double Pm10 { get; set; }
        [Display(Name = "Avg. PM 2.5")]
        public double Pm25 { get; set; }
        [Display(Name = "Avg. PM1")]
        public double Pm1 { get; set; }
        [Display(Name = "Energy Class (kWh/year/full module)")]
        public double? EnergyClass { get; set; }
        public int Id { get; set; }
    }
}
