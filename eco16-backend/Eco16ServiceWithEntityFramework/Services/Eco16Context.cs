﻿using Eco16ServiceWithEntityFramework.Models;
using Microsoft.EntityFrameworkCore;

namespace Eco16ServiceWithEntityFramework.Services
{
    public partial class Eco16Context : DbContext
    {
        public Eco16Context()
        {
        }

        public Eco16Context(DbContextOptions<Eco16Context> options)
            : base(options)
        {
        }

        public virtual DbSet<AirQuality> AirQuality { get; set; }
        public virtual DbSet<LimitStandards> LimitStandards { get; set; }
        public virtual DbSet<RoomType> RoomType { get; set; }
        public virtual DbSet<SupplyAirFilterClass> SupplyAirFilterClass { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AirQuality>(entity =>
            {
                entity.HasIndex(e => new { e.CountryCode, e.Station })
                    .HasName("UC_AirQuality")
                    .IsUnique();

                entity.Property(e => e.AveragePm1).HasColumnName("AveragePM1");

                entity.Property(e => e.AveragePm10).HasColumnName("AveragePM10");

                entity.Property(e => e.AveragePm25).HasColumnName("AveragePM2.5");

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(3);

                entity.Property(e => e.NoteOnConvertedPm10)
                    .HasColumnName("NoteOnConvertedPM10")
                    .HasMaxLength(50);

                entity.Property(e => e.NumberAndTypeOfMonitoringStations).HasMaxLength(50);

                entity.Property(e => e.Station)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LimitStandards>(entity =>
            {
                entity.HasIndex(e => e.Standard)
                    .HasName("UC_LimitStandard")
                    .IsUnique();

                entity.Property(e => e.Pm1).HasColumnName("PM1");

                entity.Property(e => e.Pm10).HasColumnName("PM10");

                entity.Property(e => e.Pm25).HasColumnName("PM2.5");

                entity.Property(e => e.Standard)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<RoomType>(entity =>
            {
                entity.Property(e => e.Pm1).HasColumnName("PM1");

                entity.Property(e => e.Pm10).HasColumnName("PM10");

                entity.Property(e => e.Pm25).HasColumnName("PM2.5");

                entity.Property(e => e.RoomType1)
                    .IsRequired()
                    .HasColumnName("RoomType")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SupplyAirFilterClass>(entity =>
            {
                entity.Property(e => e.FilterClass)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pm1).HasColumnName("PM1");

                entity.Property(e => e.Pm10).HasColumnName("PM10");

                entity.Property(e => e.Pm25).HasColumnName("PM2.5");

                entity.Property(e => e.ProductType)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
