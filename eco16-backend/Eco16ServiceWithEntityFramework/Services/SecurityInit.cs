﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Eco16ServiceWithEntityFramework.Services
{
    public static class SecurityInit
    {

        public static async Task Initialize(IServiceProvider serviceProvider, string userName, string testUserPw, string role)
        {

            // For sample purposes we are seeding 2 users both with the same password.
            // The password is set with the following command:
            // dotnet user-secrets set SeedUserPW <pw>
            // The admin user can do anything

            string adminID = await EnsureUser(serviceProvider, testUserPw, userName);
            await EnsureRole(serviceProvider, adminID, role);

        }
        private static async Task<string> EnsureUser(IServiceProvider serviceProvider,
                                                   string testUserPw, string UserName)
        {
            UserManager<IdentityUser> userManager = serviceProvider.GetService<UserManager<IdentityUser>>();
            IdentityUser user = await userManager.FindByNameAsync(UserName);
            if (user == null)
            {
                user = new IdentityUser { UserName = UserName };
                await userManager.CreateAsync(user, testUserPw);
            }

            return user.Id;
        }

        private static async Task<IdentityResult> EnsureRole(IServiceProvider serviceProvider,
                                                                      string uid, string role)
        {
            IdentityResult IR = null;
            RoleManager<IdentityRole> roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            bool roleExists = await roleManager.RoleExistsAsync(role);
            if (!roleExists)
            {
                IR = await roleManager.CreateAsync(new IdentityRole(role));
                UserManager<IdentityUser> userManager = serviceProvider.GetService<UserManager<IdentityUser>>();
                IdentityUser user = await userManager.FindByIdAsync(uid);
                await userManager.AddToRoleAsync(user, role);
            }
            return IR;
        }
    }
}
