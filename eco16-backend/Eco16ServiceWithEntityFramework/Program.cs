﻿using Eco16ServiceWithEntityFramework.Data;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Eco16ServiceWithEntityFramework
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost host = BuildWebHost(args);
            using (IServiceScope scope = host.Services.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;
                ApplicationDbContext context = services.GetRequiredService<ApplicationDbContext>();
                context.Database.Migrate();
                IConfiguration config = services.GetRequiredService<IConfiguration>();
                string username1 = config.GetValue<string>("UserID1");
                string password1 = StringCipher.Decrypt(config.GetValue<string>("Password1"));
                SecurityInit.Initialize(services, username1, password1, "admin").Wait();
                string username2 = config.GetValue<string>("UserID2");
                string password2 = StringCipher.Decrypt(config.GetValue<string>("Password2"));
                SecurityInit.Initialize(services, username2, password2, "user").Wait();
            }
            host.Run();
        }
        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                    .UseStartup<Startup>()
                    .Build();
        }

    }
}
