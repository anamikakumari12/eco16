﻿using Eco16ServiceWithEntityFramework.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Eco16ServiceWithEntityFramework.ViewModel
{
    public enum FilterTypes
    {

    }
    public class AirQualityIndexViewModel
    {
        public List<AirQuality> AirQualityList { get; set; }
        public int PageIndex { get; set; }
        public int NextPageIndex { get; set; }
        public int PreviousPageIndex { get; set; }
        public int MaxNextPage { get; set; }
        public string FilterString { get; internal set; }
        public string SelectedFilterType { get; set; }
        public List<SelectListItem> FilterTypes { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "Country", Text = "Country" },
            new SelectListItem { Value = "Station", Text = "Station" },
        };
    }
}
