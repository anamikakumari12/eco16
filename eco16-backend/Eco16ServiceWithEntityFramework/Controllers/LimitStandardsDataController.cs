﻿using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class LimitStandardsDataController : ControllerBase
    {
        private readonly Eco16Context _context;

        public LimitStandardsDataController(Eco16Context context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<List<string>> GetStandards()
        {
            List<string> standards = _context.LimitStandards.Select(limit => limit.Standard).ToList();
            return standards;
        }

        [HttpGet]
        public ActionResult<LimitStandards> GetLimitValuesForStandard(string standard)
        {
            LimitStandards limitValuesForStandard = _context.LimitStandards.FirstOrDefault(l => l.Standard == standard);
            if (limitValuesForStandard == null)
            {
                return NotFound();
            }
            return limitValuesForStandard;
        }
    }
}