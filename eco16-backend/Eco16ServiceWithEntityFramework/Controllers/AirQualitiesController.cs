﻿using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Eco16ServiceWithEntityFramework.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Authorize(Roles = "admin")]
    [DisableCors]
    public class AirQualitiesController : Controller
    {
        private readonly Eco16Context _context;

        public object AirQualityViewModel { get; private set; }

        public AirQualitiesController(Eco16Context context)
        {
            _context = context;
        }

        // GET: AirQualities
        public async Task<IActionResult> Index(int? PageIndex, string FilterString, string SelectedFilterType)
        {
            if (!PageIndex.HasValue)
            {
                PageIndex = 0;
            }
            int pageSize = 10;

            AirQualityIndexViewModel airQualityIndexViewModel = new AirQualityIndexViewModel
            {
                SelectedFilterType = SelectedFilterType
            };
            IQueryable<AirQuality> filteredAirQualitiesList = null;
            if (string.IsNullOrEmpty(FilterString))
            {
                filteredAirQualitiesList = _context.AirQuality;
            }
            else
            {
                if (airQualityIndexViewModel.SelectedFilterType == airQualityIndexViewModel.FilterTypes[0].Value)
                {
                    // Country
                    filteredAirQualitiesList = _context.AirQuality.Where(air => air.Country.Contains(FilterString));
                }
                else
                {
                    filteredAirQualitiesList = _context.AirQuality.Where(air => air.Station.Contains(FilterString));
                }
            }

            IQueryable<AirQuality> airQualitiesList = filteredAirQualitiesList.OrderByDescending(air => air.Id).Skip((PageIndex.Value) * pageSize).Take(pageSize);
            System.Collections.Generic.List<AirQuality> airQs = await airQualitiesList.ToListAsync();
            airQualityIndexViewModel.AirQualityList = airQs;
            airQualityIndexViewModel.PageIndex = PageIndex.Value;
            airQualityIndexViewModel.NextPageIndex = PageIndex.Value + 1;
            airQualityIndexViewModel.PreviousPageIndex = PageIndex.Value - 1;
            airQualityIndexViewModel.MaxNextPage = Convert.ToInt32(Math.Floor(filteredAirQualitiesList.Count() / 10m)) + 1;
            airQualityIndexViewModel.FilterString = FilterString;
            return View(airQualityIndexViewModel);
        }

        // GET: AirQualities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AirQuality airQuality = await _context.AirQuality
                .FirstOrDefaultAsync(m => m.Id == id);
            if (airQuality == null)
            {
                return NotFound();
            }

            return View(airQuality);
        }

        // GET: AirQualities/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AirQualities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CountryCode,Country,Station,Year,NumberAndTypeOfMonitoringStations,NoteOnConvertedPm10,AveragePm10,AveragePm25,AveragePm1,YearOfMeasurement,Id")] AirQuality airQuality)
        {
            if (ModelState.IsValid)
            {
                _context.Add(airQuality);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(airQuality);
        }

        // GET: AirQualities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AirQuality airQuality = await _context.AirQuality.FindAsync(id);
            if (airQuality == null)
            {
                return NotFound();
            }
            return View(airQuality);
        }

        // POST: AirQualities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Year,NumberAndTypeOfMonitoringStations,NoteOnConvertedPm10,AveragePm10,AveragePm25,AveragePm1,YearOfMeasurement,Id")] AirQuality airQuality)
        {
            if (id != airQuality.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(airQuality);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AirQualityExists(airQuality.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(airQuality);
        }

        // GET: AirQualities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AirQuality airQuality = await _context.AirQuality
                .FirstOrDefaultAsync(m => m.Id == id);
            if (airQuality == null)
            {
                return NotFound();
            }

            return View(airQuality);
        }

        // POST: AirQualities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            AirQuality airQuality = await _context.AirQuality.FindAsync(id);
            _context.AirQuality.Remove(airQuality);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AirQualityExists(int id)
        {
            return _context.AirQuality.Any(e => e.Id == id);
        }
    }
}
