﻿using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class RoomTypesDataController : ControllerBase
    {
        private readonly Eco16Context _context;

        public RoomTypesDataController(Eco16Context context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<List<string>> GetRoomTypes()
        {
            List<string> roomTypes = _context.RoomType.Select(room => room.RoomType1).ToList();
            return roomTypes;
        }

        [HttpGet]
        public ActionResult<RoomType> GetIndoorAirBasedOnRoomType(string roomType)
        {
            RoomType indoorAir = _context.RoomType.FirstOrDefault(i => i.RoomType1 == roomType);
            if (indoorAir == null)
            {
                return NotFound();
            }
            return indoorAir;
        }
    }
}