﻿using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Authorize(Roles = "admin")]
    [DisableCors]
    public class SupplyAirFilterClassesController : Controller
    {
        private readonly Eco16Context _context;

        public SupplyAirFilterClassesController(Eco16Context context)
        {
            _context = context;
        }

        // GET: SupplyAirFilterClasses
        public async Task<IActionResult> Index()
        {
            return View(await _context.SupplyAirFilterClass.ToListAsync());
        }

        // GET: SupplyAirFilterClasses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SupplyAirFilterClass supplyAirFilterClass = await _context.SupplyAirFilterClass
                .FirstOrDefaultAsync(m => m.Id == id);
            if (supplyAirFilterClass == null)
            {
                return NotFound();
            }

            return View(supplyAirFilterClass);
        }

        // GET: SupplyAirFilterClasses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SupplyAirFilterClasses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductType,FilterClass,Pm10,Pm25,Pm1,EnergyClass,Id")] SupplyAirFilterClass supplyAirFilterClass)
        {
            if (ModelState.IsValid)
            {
                _context.Add(supplyAirFilterClass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(supplyAirFilterClass);
        }

        // GET: SupplyAirFilterClasses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SupplyAirFilterClass supplyAirFilterClass = await _context.SupplyAirFilterClass.FindAsync(id);
            if (supplyAirFilterClass == null)
            {
                return NotFound();
            }
            return View(supplyAirFilterClass);
        }

        // POST: SupplyAirFilterClasses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductType,FilterClass,Pm10,Pm25,Pm1,EnergyClass,Id")] SupplyAirFilterClass supplyAirFilterClass)
        {
            if (id != supplyAirFilterClass.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(supplyAirFilterClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SupplyAirFilterClassExists(supplyAirFilterClass.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(supplyAirFilterClass);
        }

        // GET: SupplyAirFilterClasses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SupplyAirFilterClass supplyAirFilterClass = await _context.SupplyAirFilterClass
                .FirstOrDefaultAsync(m => m.Id == id);
            if (supplyAirFilterClass == null)
            {
                return NotFound();
            }

            return View(supplyAirFilterClass);
        }

        // POST: SupplyAirFilterClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            SupplyAirFilterClass supplyAirFilterClass = await _context.SupplyAirFilterClass.FindAsync(id);
            _context.SupplyAirFilterClass.Remove(supplyAirFilterClass);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SupplyAirFilterClassExists(int id)
        {
            return _context.SupplyAirFilterClass.Any(e => e.Id == id);
        }
    }
}
