﻿using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class AirDataController : ControllerBase
    {
        private Eco16Context _context;

        public AirDataController(Eco16Context context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<List<string>> GetCountries()
        {
            List<string> uniqueCountriesData = _context.AirQuality.OrderBy(air => air.Country).Select(air => air.Country).Distinct().ToList();
            return uniqueCountriesData;
        }

        [HttpGet]
        public ActionResult<List<string>> GetStations(string country)
        {
            List<string> stationsInCountry = _context.AirQuality.Where(air => air.Country == country).OrderBy(air => air.Station)
                .Select(air => air.Station).ToList();
            return stationsInCountry;
        }

        [HttpGet]
        public ActionResult<AirQuality> GetOutDoorAirQuality(string country, string station)
        {
            AirQuality outDoorAirQuality = _context.AirQuality
                .FirstOrDefault(air => air.Country == country && air.Station == station);
            if (outDoorAirQuality == null)
            {
                return NotFound();
            }
            return outDoorAirQuality;
        }
    }
}