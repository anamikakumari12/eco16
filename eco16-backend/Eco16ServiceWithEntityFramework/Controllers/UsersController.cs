﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Route("api/[controller]/[action]")]
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Authenticate([FromBody]User userParam)
        {
            var user = await _userService.Authenticate(userParam.Username, userParam.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }
    }
}