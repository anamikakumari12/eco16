﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Authorize(Roles = "admin")]
    [DisableCors]
    public class LimitStandardsController : Controller
    {
        private readonly Eco16Context _context;

        public LimitStandardsController(Eco16Context context)
        {
            _context = context;
        }

        // GET: LimitStandards
        public async Task<IActionResult> Index()
        {
            return View(await _context.LimitStandards.ToListAsync());
        }

        // GET: LimitStandards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var limitStandards = await _context.LimitStandards
                .FirstOrDefaultAsync(m => m.Id == id);
            if (limitStandards == null)
            {
                return NotFound();
            }

            return View(limitStandards);
        }

        // GET: LimitStandards/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LimitStandards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Standard,Pm10,Pm25,Pm1,Id")] LimitStandards limitStandards)
        {
            if (ModelState.IsValid)
            {
                _context.Add(limitStandards);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(limitStandards);
        }

        // GET: LimitStandards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var limitStandards = await _context.LimitStandards.FindAsync(id);
            if (limitStandards == null)
            {
                return NotFound();
            }
            return View(limitStandards);
        }

        // POST: LimitStandards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Standard,Pm10,Pm25,Pm1,Id")] LimitStandards limitStandards)
        {
            if (id != limitStandards.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(limitStandards);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LimitStandardsExists(limitStandards.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(limitStandards);
        }

        // GET: LimitStandards/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var limitStandards = await _context.LimitStandards
                .FirstOrDefaultAsync(m => m.Id == id);
            if (limitStandards == null)
            {
                return NotFound();
            }

            return View(limitStandards);
        }

        // POST: LimitStandards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var limitStandards = await _context.LimitStandards.FindAsync(id);
            _context.LimitStandards.Remove(limitStandards);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LimitStandardsExists(int id)
        {
            return _context.LimitStandards.Any(e => e.Id == id);
        }
    }
}
