﻿using Eco16ServiceWithEntityFramework.Models;
using Eco16ServiceWithEntityFramework.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Eco16ServiceWithEntityFramework.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class SupplyAirFilterDataController : ControllerBase
    {
        private readonly Eco16Context _context;

        public SupplyAirFilterDataController(Eco16Context context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<List<string>> GetProductTypes()
        {
            List<string> products = _context.SupplyAirFilterClass.Select(p => p.ProductType).Distinct().ToList();
            return products;
        }

        [HttpGet]
        public ActionResult<List<string>> GetFilterClass(string productType)
        {
            List<string> filterClasses = _context.SupplyAirFilterClass.Where(p => p.ProductType == productType)
                .Select(p => p.FilterClass).ToList();
            return filterClasses;
        }

        [HttpGet]
        public ActionResult<SupplyAirFilterClass> GetFilterDataForProductTypeAndFilter(string productType, string filterClass)
        {
            SupplyAirFilterClass filterData = _context.SupplyAirFilterClass
                .FirstOrDefault(f => f.ProductType == productType && f.FilterClass == filterClass);
            if (filterData == null)
            {
                return NotFound();
            }
            return filterData;
        }
    }
}