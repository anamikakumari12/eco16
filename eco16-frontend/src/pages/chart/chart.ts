import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';

/**
 * Generated class for the ChartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chart',
  templateUrl: 'chart.html',
})
export class ChartPage {

  stationName: string;
  outdoorAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  supplyAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  indoorAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  limitValue: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  @ViewChild('barCanvas') barCanvas;

  barChart: Chart;

  constructor(public navCtrl: NavController, public navParams: NavParams, public plt: Platform) {
  }

  ionViewDidLoad() {
    this.outdoorAirQuality = this.navParams.get("outdoorAirQuality");
    this.supplyAirQuality = this.navParams.get("supplyAirQuality");
    this.indoorAirQuality = this.navParams.get("indoorAirQuality");
    this.stationName = this.navParams.get("stationName");
    this.limitValue = this.navParams.get("limitValue");

    this.barChart = new Chart(this.barCanvas.nativeElement, {

      type: 'bar',
      data: {
        labels: ["PM10", "PM2.5", "PM1"],
        datasets: [
          {
            type: 'line',
            label: 'limit Value',
            data: [this.limitValue.pm10, this.limitValue.pmTwoAndHalf],
            fill: false,
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            borderColor: 'rgba(0, 0, 0, 0.8)',
            borderWidth: 2,
            hoverBorderWidth: 2,
            pointRadius: this.plt.width() / 9.5,
            pointHitRadius: this.plt.width() / 9.5,
            pointHoverRadius: this.plt.width() / 9.5,
            showLine: false,
            steppedLine: true,
            pointStyle: 'line'
          }, {
            label: 'Out door Air Quality',
            data: [this.outdoorAirQuality.pm10, this.outdoorAirQuality.pmTwoAndHalf, this.outdoorAirQuality.pm1],
            backgroundColor: 'rgba(255, 99, 132, 0.6)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
          },
          {
            label: 'Supply Air Quality',
            data: [this.supplyAirQuality.pm10, this.supplyAirQuality.pmTwoAndHalf, this.supplyAirQuality.pm1],
            backgroundColor: 'rgba(54, 162, 235, 0.6)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1,
          },
          {
            label: 'Indoor Air Quality',
            data: [this.indoorAirQuality.pm10, this.indoorAirQuality.pmTwoAndHalf, this.indoorAirQuality.pm1],
            backgroundColor: 'rgba(255, 206, 86, 0.6)',
            borderColor: 'rgba(255, 206, 86, 1)',
            borderWidth: 1,
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          display: true,
          position: 'bottom'
        },
        layout: {
          padding: {
            top: 30
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              maxTicksLimit: 1000,
              padding: 40,
            },
            scaleLabel: {
              display: true,
              labelString: 'PM concentration, µg/m³'
            }
          }]
        },
        plugins: {
          datalabels: {
            anchor: 'end',
            align: 'center',
            formatter: Math.round,
            font: {
              weight: 'bold'
            },
            display: function (context) {
              if (context.dataset.label === 'limit Value') {
                return false;
              }
              return true;
            }
          }
        }
      }
    });
  }

}
