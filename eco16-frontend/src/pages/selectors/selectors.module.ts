import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectorsPage } from './selectors';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SelectorsPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectorsPage),
    ComponentsModule
  ],
})
export class SelectorsPageModule { }
