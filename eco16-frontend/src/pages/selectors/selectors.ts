import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { DatacontextProvider } from '../../providers/datacontext/datacontext';
import { ChartPage } from '../chart/chart';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
/**
 * Generated class for the SelectorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selectors',
  templateUrl: 'selectors.html',
})
export class SelectorsPage implements OnInit {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public datacontext: DatacontextProvider,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public toastCtrl: ToastController) {
  }


  selectedCountry = undefined;
  selectedStation = undefined;

  countries: Array<string>;
  stations: Array<string>;



  outdoorAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  supplyAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  indoorAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  limitValue: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  key: string = 'selectedCountry';
  storedCountry: string = '';
  ionViewDidLoad() {

  }

  showLoader(text: string = 'Fetching data from server...'): Loading {
    const loader = this.loadingCtrl.create({
      content: text,
    });
    loader.present();
    return loader;
  }

  hideloader(loader: Loading): void {
    loader.dismiss();
  }

  async ngOnInit(): Promise<void> {
    const token = await this.storage.get('authHeader');
    this.datacontext.AuthorizationHeader = token;
    const loader = this.showLoader();
    this.datacontext.getCountries().then(data => {
      this.countries = data;

      // Adding this code on 3rd July, 2018 to check if the country is already saved or not.
      this.storage.get(this.key).then(
        (data) => {
          if (data) {
            this.selectedCountry = data;
            this.storedCountry = data;
            this.handleCountryChange();
          }
          this.hideloader(loader);
        },
        error => console.error(error)
      );

    }, (error) => {
      console.error(error);
      this.hideloader(loader);
      this.navCtrl.setRoot(LoginPage);
    });
  }

  handleCountryChange(): void {
    const loader = this.showLoader();
    // Save the new country preference
    if (this.storedCountry !== this.selectedCountry) {
      this.storage.set(this.key, this.selectedCountry).then(
        _ => {
          // Show toastr that preference is saved
          const toast = this.toastCtrl.create({
            message: 'Country preference saved sucessfully',
            duration: 3000
          });
          toast.present();
        },
        error => console.log(error)
      );
    }
    this.selectedStation = '';
    this.datacontext.getStations(this.selectedCountry).then(data => {
      this.stations = data;
      this.hideloader(loader);
    }, (error) => {
      console.error(error);
      this.hideloader(loader);
      this.navCtrl.push(LoginPage);
    });
  }

  handleGenerateGraphButtonClick(): void {
    this.navCtrl.push(ChartPage, {
      outdoorAirQuality: this.outdoorAirQuality,
      supplyAirQuality: this.supplyAirQuality,
      indoorAirQuality: this.indoorAirQuality,
      limitValue: this.limitValue,
      stationName: this.selectedStation
    });
  }

  handleStationChange(): void {
    this.outdoorAirQuality = this.supplyAirQuality = this.indoorAirQuality = this.limitValue = undefined;
  }

  reset(): void {
    this.outdoorAirQuality = this.supplyAirQuality = this.indoorAirQuality = this.limitValue = undefined;
    this.selectedStation = '';
    this.storage.get(this.key).then(
      (data) => {
        if (data) {
          this.selectedCountry = data;
        }
      },
      error => console.error(error)
    );
  }

}
