import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SelectorsPage } from '../selectors/selectors';
import { DatacontextProvider } from '../../providers/datacontext/datacontext';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {


  constructor(public navCtrl: NavController, public datacontext: DatacontextProvider) {
  }

  ngOnInit(): void {

    setTimeout(() => {
      this.navCtrl.setRoot(SelectorsPage);
    }, 3000);

  }

}
