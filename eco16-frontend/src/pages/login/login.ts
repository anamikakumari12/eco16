import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { DatacontextProvider } from '../../providers/datacontext/datacontext';
import { Storage } from '@ionic/storage';
import { SelectorsPage } from '../selectors/selectors';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username: string;
  password: string;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storage: Storage, public datacontext: DatacontextProvider, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async login() {
    try {
      var ret = await this.datacontext.getAuthorizationToken(this.username, this.password);
      console.log(ret);
      this.storage.set('authHeader', ret.token).then(() => {
        this.navCtrl.setRoot(SelectorsPage);
      }, (error) => {
        console.log(error);
      });
    } catch (error) {
      await this.storage.remove('authHeader');
      this.presentToast();
    }
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Username or password is incorrect or some other error has occured',
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }
}
