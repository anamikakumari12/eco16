import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SelectorsPage } from '../pages/selectors/selectors';
import { SelectorsPageModule } from '../pages/selectors/selectors.module';
import { DatacontextProvider } from '../providers/datacontext/datacontext';
import { HttpClientModule } from '@angular/common/http';
import { ChartPageModule } from '../pages/chart/chart.module';
import { ChartPage } from '../pages/chart/chart';
import { IonicStorageModule } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    SelectorsPageModule,
    ChartPageModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SelectorsPage,
    ChartPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    DatacontextProvider
  ]
})
export class AppModule { }
