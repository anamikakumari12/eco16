import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the DatacontextProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatacontextProvider {

  private hostUrl = "http://eco16dev-env.cpkir7j2zn.us-east-2.elasticbeanstalk.com/";
  // private hostUrl = "https://localhost:44355/";

  private username: string = "User@eco16";
  private password = atob("VXNlcjkhQG1hbm4raA==");
  private authorizationHeader;;

  constructor(public http: HttpClient) {
  }


  public set AuthorizationHeader(v: string) {
    this.authorizationHeader = 'Bearer ' + v;
  }


  getAuthorizationToken(username: string, password: string) {
    const url = this.hostUrl + '/api/Users/Authenticate';
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
      })
    };
    const body = {
      username: username,
      password: password
    };
    return this.http.post<any>(url, body, httpOptions).toPromise();
  }

  getCountries(): Promise<any> {

    const url = this.hostUrl + '/api/airData/getCountries';
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getStations(country: string): Promise<any> {

    const url = this.hostUrl + `/api/airData/GetStations/?country=${country}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getOutDoorAirQuality(country: string, station: string): Promise<any> {

    const url = this.hostUrl + `/api/airData/GetOutDoorAirQuality/?country=${country}&station=${station}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }


  getStandards(): Promise<any> {

    const url = this.hostUrl + '/api/LimitStandardsData/GetStandards';
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getLimitValuesForStandard(standard: string): Promise<any> {

    const url = this.hostUrl + `/api/LimitStandardsData/GetLimitValuesForStandard/?standard=${standard}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getProductTypes(): Promise<any> {
    const url = this.hostUrl + `/api/SupplyAirFilterData/GetProductTypes`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getFilterClass(productType: string): Promise<any> {
    const url = this.hostUrl + `/api/SupplyAirFilterData/GetFilterClass/?productType=${productType}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getFilterDataForProductTypeAndFilter(productType: string, filterClass: string): Promise<any> {
    const url = this.hostUrl + `/api/SupplyAirFilterData/GetFilterDataForProductTypeAndFilter/` +
      `?productType=${productType}&filterClass=${filterClass}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getRoomTypes(): Promise<any> {
    const url = this.hostUrl + `/api/RoomTypesData/GetRoomTypes`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

  getIndoorAirBasedOnRoomType(roomType: string): Promise<any> {
    const url = this.hostUrl + `/api/RoomTypesData/GetIndoorAirBasedOnRoomType/?roomType=${roomType}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json;odata=verbose',
        'Authorization': this.authorizationHeader
      })
    };
    return this.http.get<any>(url, httpOptions).toPromise();
  }

}
