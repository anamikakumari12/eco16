import { Component, Input, OnInit, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { DatacontextProvider } from '../../providers/datacontext/datacontext';
import { LoadingController, Loading } from 'ionic-angular';

/**
 * Generated class for the OutDoorAirComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'out-door-air',
  templateUrl: 'out-door-air.html'
})
export class OutDoorAirComponent implements OnInit, OnChanges {


  @Input() country: string;
  @Input() station: string;

  @Output() onValueReceived = new EventEmitter<{
    pm10?: number,
    pmTwoAndHalf?: number,
    pm1?: number
  }>();

  pm10: number;
  pmTwoAndHalf: number;
  pm1: number;

  constructor(public datacontext: DatacontextProvider, public loadingCtrl: LoadingController) {

  }

  showLoader(text: string = 'Fetching data from server...'): Loading {
    const loader = this.loadingCtrl.create({
      content: text,
    });
    loader.present();
    return loader;
  }

  hideloader(loader: Loading): void {
    loader.dismiss();
  }

  ngOnInit(): void {
    this.getOutDoorAirQuality();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getOutDoorAirQuality();
  }

  getOutDoorAirQuality(): void {
    if (this.country && this.station) {
      const loader = this.showLoader();
      this.onValueReceived.emit({});
      this.datacontext.getOutDoorAirQuality(this.country, this.station).then(
        data => {
          let parsedData = data;
          this.pm1 = parsedData.averagePm1;
          this.pmTwoAndHalf = parsedData.averagePm25;
          this.pm10 = parsedData.averagePm10;

          this.onValueReceived.emit({
            pm1: this.pm1,
            pmTwoAndHalf: this.pmTwoAndHalf,
            pm10: this.pm10
          });
          this.hideloader(loader);
        },
        error => { console.error(error);  this.hideloader(loader); }
      );
    } else {
      this.onValueReceived.emit(null);
    }
  }

}
