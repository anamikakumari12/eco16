import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { DatacontextProvider } from '../../providers/datacontext/datacontext';
import { Loading, LoadingController } from 'ionic-angular';

/**
 * Generated class for the IndoorAirComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'indoor-air',
  templateUrl: 'indoor-air.html'
})
export class IndoorAirComponent implements OnInit, OnChanges {

  roomTypes: Array<string>;
  selectedRoomType: string = "";

  PM10: number;
  PM2AndHalf: number;
  PM1: number;


  @Input() supplyAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  @Output() onValueReceived = new EventEmitter<{
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  }>();

  constructor(public datacontext: DatacontextProvider, public loadingCtrl: LoadingController) {
  }



  showLoader(text: string = 'Fetching data from server...'): Loading {
    const loader = this.loadingCtrl.create({
      content: text,
    });
    loader.present();
    return loader;
  }

  hideloader(loader: Loading): void {
    loader.dismiss();
  }


  ngOnInit(): void {
    this.getRoomTypes();
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.getRoomTypes();
  }

  getRoomTypes(): void {
    const loader = this.showLoader();
    if (!this.selectedRoomType) {
      this.selectedRoomType = '';
    }
    this.PM1 = null;
    this.PM10 = null;
    this.PM2AndHalf = null;
    this.datacontext.getRoomTypes().then(
      data => {
        this.roomTypes = data; this.hideloader(loader);
        if (this.selectedRoomType) {
          this.handleRoomTypeChange();
        }
      },
      error => { console.error(error); this.hideloader(loader); }
    );
  }

  handleRoomTypeChange(): void {
    if (this.selectedRoomType && this.supplyAirQuality) {
      const loader = this.showLoader();
      this.datacontext.getIndoorAirBasedOnRoomType(this.selectedRoomType).then(
        data => {
          this.PM10 = this.supplyAirQuality.pm10 + data.pm10;
          this.PM2AndHalf = this.supplyAirQuality.pmTwoAndHalf + data.pm25;
          this.PM1 = this.supplyAirQuality.pm1 + data.pm1;

          this.onValueReceived.emit({
            pm1: this.PM1,
            pmTwoAndHalf: this.PM2AndHalf,
            pm10: this.PM10
          });
          this.hideloader(loader);
        },
        error => { console.error(error); this.hideloader(loader); }
      );
    } else {
      this.onValueReceived.emit(null);
    }
  }
}
