import { Component, OnInit, Output, EventEmitter, SimpleChanges, OnChanges, Input } from '@angular/core';
import { DatacontextProvider } from '../../providers/datacontext/datacontext';
import { LoadingController, Loading } from 'ionic-angular';

/**
 * Generated class for the LimitValueComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'limit-value',
  templateUrl: 'limit-value.html'
})
export class LimitValueComponent implements OnInit, OnChanges {

  @Input() country: string;
  @Input() station: string;

  selectedStandard: string = "";
  standards: Array<string>;

  limitValuePM1?: number;
  limitValuePM2AndHalf: number
  limitValuePM10: number


  @Output() onValueReceived = new EventEmitter<{
    pm10?: number,
    pmTwoAndHalf?: number,
    pm1?: number
  }>();

  constructor(public datacontext: DatacontextProvider
    , public loadingCtrl: LoadingController) {

  }

  showLoader(text: string = 'Fetching data from server...'): Loading {
    const loader = this.loadingCtrl.create({
      content: text,
    });
    loader.present();
    return loader;
  }

  hideloader(loader: Loading): void {
    loader.dismiss();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  ngOnInit(): void {
    const loader = this.showLoader();
    this.selectedStandard = '';
    this.limitValuePM10 = null;
    this.limitValuePM1 = null;
    this.limitValuePM2AndHalf = null;
    this.datacontext.getStandards().then(data => {
      this.standards = data;
      this.hideloader(loader);
    }, (error) => {
      console.error(error);  this.hideloader(loader);
    });
  }

  handleStandardChange(): void {
    if (this.selectedStandard) {
      const loader = this.showLoader();
      this.datacontext.getLimitValuesForStandard(this.selectedStandard).then(
        data => {
          let parsedData = data;
          this.limitValuePM10 = parsedData.pm10;
          this.limitValuePM2AndHalf = parsedData.pm25;
          this.limitValuePM1 = parsedData.pm1;


          this.onValueReceived.emit({
            pm1: this.limitValuePM1,
            pmTwoAndHalf: this.limitValuePM2AndHalf,
            pm10: this.limitValuePM10
          });

          this.hideloader(loader);
        },
        error => { console.error(error);   this.hideloader(loader);}
      );
    } else {

      this.onValueReceived.emit(null);
    }
  }

}
