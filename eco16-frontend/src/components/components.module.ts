import { NgModule } from '@angular/core';
import { OutDoorAirComponent } from './out-door-air/out-door-air';
import { IonicPageModule } from 'ionic-angular';
import { SupplyAirComponent } from './supply-air/supply-air';
import { IndoorAirComponent } from './indoor-air/indoor-air';
import { LimitValueComponent } from './limit-value/limit-value';

@NgModule({
	declarations: [OutDoorAirComponent,
    SupplyAirComponent,
    IndoorAirComponent,
    LimitValueComponent],
	imports: [IonicPageModule.forChild(OutDoorAirComponent)],
	exports: [OutDoorAirComponent,
    SupplyAirComponent,
    IndoorAirComponent,
    LimitValueComponent]
})
export class ComponentsModule { }
