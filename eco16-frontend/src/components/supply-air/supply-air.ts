import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DatacontextProvider } from '../../providers/datacontext/datacontext';
import { LoadingController, Loading } from 'ionic-angular';

/**
 * Generated class for the SupplyAirComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'supply-air',
  templateUrl: 'supply-air.html'
})
export class SupplyAirComponent implements OnInit, OnChanges {

  selectedProduct: string;
  products: Array<string>;

  selectedFilterClass: string;
  filterClasses: Array<string>;

  PM10: number;
  PM2AndHalf: number;
  PM1: number;
  EnergyClass: number;

  @Input() outdoorAirQuality: {
    pm10: number,
    pmTwoAndHalf: number,
    pm1: number
  };

  @Output() onValueReceived = new EventEmitter<{
    pm10?: number,
    pmTwoAndHalf?: number,
    pm1?: number
  }>();


  constructor(public datacontext: DatacontextProvider, public loadingCtrl: LoadingController) {

  }


  showLoader(text: string = 'Fetching data from server...'): Loading {
    const loader = this.loadingCtrl.create({
      content: text,
    });
    loader.present();
    return loader;
  }

  hideloader(loader: Loading): void {
    loader.dismiss();
  }

  ngOnInit(): void {
    this.getProductTypes();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getProductTypes();
  }

  getProductTypes(): void {
    const loader = this.showLoader();
    this.selectedProduct = '';
    this.PM1 = null;
    this.PM10 = null;
    this.PM2AndHalf = null;
    this.EnergyClass = null;
    this.datacontext.getProductTypes().then(
      data => {
        this.products = data;
        this.filterClasses = null;
        this.hideloader(loader);
      },
      error => { console.error(error);  this.hideloader(loader); }
    );
  }

  handleProductChange(): void {
    const loader = this.showLoader();
    this.selectedFilterClass = null;
    this.datacontext.getFilterClass(this.selectedProduct).then(
      data => {
        this.filterClasses = data;
        this.hideloader(loader);
      },
      error => { console.error(error);  this.hideloader(loader); }
    )
  }

  handleSelectedFilterClassChange(): void {
    if (this.selectedProduct && this.selectedFilterClass) {
      this.datacontext.getFilterDataForProductTypeAndFilter(this.selectedProduct, this.selectedFilterClass).then(
        data => {
          this.PM10 = this.outdoorAirQuality.pm10 * (1 - data.pm10);
          let intermediatePM2AndHalf = this.outdoorAirQuality.pmTwoAndHalf * (1 - data.pm25);
          this.PM2AndHalf = intermediatePM2AndHalf > this.PM10 ? this.PM10 : intermediatePM2AndHalf;
          let intermediatePM1 = this.outdoorAirQuality.pm1 * (1 - data.pm1);
          this.PM1 = this.PM2AndHalf < intermediatePM1 ? this.PM2AndHalf : intermediatePM1;
          this.EnergyClass = Number(data.energyClass) === -1 ? null : data.energyClass;

          this.onValueReceived.emit({
            pm1: this.PM1,
            pmTwoAndHalf: this.PM2AndHalf,
            pm10: this.PM10
          });
        },
        error => { console.error(error) }
      )
    } else {
      // Emit nothing indicating new changes
      this.onValueReceived.emit(null);
    }
  }
}
